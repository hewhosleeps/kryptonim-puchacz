import React from 'react';
import styled from 'styled-components';

interface PaperProps {
  children: React.ReactNode;
};

const StyledPaper = styled.div`
  background: #fff;
  min-height: 100%;
  max-width: 1242px;
  max-width: 1000px;
  margin: 0 auto;
  padding: 24px;
`;

const Paper: React.FC<PaperProps> = ({ children }) => (
  <StyledPaper>
    {children}
  </StyledPaper>
);

export default Paper;