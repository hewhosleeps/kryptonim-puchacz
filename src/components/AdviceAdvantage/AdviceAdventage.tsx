import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import Paper from '../Paper/Paper';
import AdviceAdvantageFilter from '../AdviceAdvantageFilter/AdviceAdvantageFilter';
import AdviceAdvantageResults from '../AdviceAdvantageResults/AdviceAdvantageResults';
import { ApplicationState } from '../../store';
import { fetchPortfolioRequested } from '../../store/portfolio/actions';
import { PortfolioData } from '../../store/portfolio/types';

interface AdviceAdvantageProps {
  fetchPortfolio: Function,
  portfolioData: PortfolioData | null,
}

const AdviceAdvantage: React.FC<AdviceAdvantageProps> = ({ fetchPortfolio, portfolioData }) => (
  <Paper>
    <AdviceAdvantageFilter onSubmit={fetchPortfolio} />
    {portfolioData && <AdviceAdvantageResults />}
  </Paper>
)

const mapStateToProps = (state: ApplicationState) => ({
  loading: state.portfolio.loading,
  portfolioData: state.portfolio.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(
  {
    fetchPortfolio: fetchPortfolioRequested,
  },
  dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AdviceAdvantage);
