import React from 'react';
import PieChart from './PieChart';
import styled from 'styled-components';

const data = {
  before: [
    { name: 'Group A', value: 400, color: '#0088FE' },
    { name: 'Group B', value: 300, color: '#00C49F' },
    { name: 'Group C', value: 300, color: '#FFBB28' },
    { name: 'Group D', value: 200, color: '#FF8042' },
  ],
  after: [
    { name: 'Group A', value: 120, color: '#0088FE' },
    { name: 'Group B', value: 200, color: '#00C49F' },
    { name: 'Group C', value: 500, color: '#FFBB28' },
    { name: 'Group D', value: 20, color: '#FF8042' },
  ],
};

const Grid = styled.div`
  display: grid;
  grid-template-columns: 50% 50%;
  justify-items: center;
`;

export default function TwoPieCharts() {
  return (
    <Grid>
      <PieChart data={data.before} width={600} height={400} />
      <PieChart data={data.after} width={600} height={400} />
    </Grid>
  );
}
