export const FETCH_PORTFOLIO_REQUESTED = '@@layout/FETCH_PORTFOLIO_REQUESTED';
export type FETCH_PORTFOLIO_REQUESTED = typeof FETCH_PORTFOLIO_REQUESTED;

export const FETCH_PORTFOLIO_SUCCESS = '@@layout/FETCH_PORTFOLIO_SUCCESS';
export type FETCH_PORTFOLIO_SUCCESS = typeof FETCH_PORTFOLIO_SUCCESS;

export interface FetchPortfolioRequested {
  type: FETCH_PORTFOLIO_REQUESTED,
  payload: {
    months: number;
    risk: number;
  }
}

export interface FetchPortfolioSuccess {
  type: FETCH_PORTFOLIO_SUCCESS,
  payload: {
    data: object;
  };
}

 export const fetchPortfolioRequested = (months: number, risk: number): FetchPortfolioRequested => ({
  type: FETCH_PORTFOLIO_REQUESTED,
  payload: {
    months,
    risk,
  }
});

export type PortfolioActions = FetchPortfolioRequested | FetchPortfolioSuccess;
