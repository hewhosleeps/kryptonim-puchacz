import mockData from './mockData';

export const fetchPortfolio = () => () => new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve({ data: mockData });
  }, 2000);
});