export interface PortfolioData {

}

export interface PortfolioState {
  loading: boolean,
  data: PortfolioData | null,
};