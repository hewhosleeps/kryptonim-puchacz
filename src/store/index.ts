import { combineReducers, Dispatch, Action, AnyAction } from 'redux';
import { all, fork } from 'redux-saga/effects';
import { connectRouter, RouterState } from 'connected-react-router';
import { History } from 'history';

import { PortfolioState, portfolioReducer } from './portfolio';
import portfolioSaga from './portfolio/sagas';

export interface ApplicationState {
  portfolio: PortfolioState;
  router: RouterState;
}

export interface ConnectedReduxProps<A extends Action = AnyAction> {
  dispatch: Dispatch<A>;
}

export const createRootReducer = (history: History) =>
  combineReducers({
    portfolio: portfolioReducer,
    router: connectRouter(history),
  });

export function* rootSaga() {
  yield all([fork(portfolioSaga)])
}
