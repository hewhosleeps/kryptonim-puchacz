export interface RadioGroupOption {
  value: string | number;
  label: string;
};